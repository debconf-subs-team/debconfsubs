1
00:00:06,049 --> 00:00:07,429
Thanks a lot for the invitation.

2
00:00:08,240 --> 00:00:11,898
It's definitely not my time of the day so
excuse me if I'm a little bit…

3
00:00:13,400 --> 00:00:14,905
Right, next to the track.

4
00:00:16,292 --> 00:00:20,186
I'm always really happy to speak
in front of people

5
00:00:20,186 --> 00:00:25,470
who are dealing with IT and stuff
because I'm so much not an IT person.

6
00:00:25,673 --> 00:00:28,759
You saw that I wasn't even able to start
my presentation alone.

7
00:00:30,625 --> 00:00:31,435
I'm from CADUS.

8
00:00:32,004 --> 00:00:37,401
CADUS is a humanitarian NGO that was
founded 3 years ago in Berlin.

9
00:00:38,294 --> 00:00:45,158
It was founded out of a kind of subculture
that is strongly related to the CCC, but

10
00:00:45,158 --> 00:00:47,999
to the musical and festival subculture
as well.

11
00:00:51,659 --> 00:00:55,762
What I want to talk about today is
our crisis response makerspace in Berlin.

12
00:00:56,372 --> 00:01:00,965
Holger (Levsen) invited me after we saw
each other again at Datengarten in Berlin

13
00:01:00,965 --> 00:01:03,529
and he asked if I can make the presentation
in english as well.

14
00:01:03,529 --> 00:01:08,119
My english is shit, so please excuse me if
I have to search some words

15
00:01:08,119 --> 00:01:09,541
from time to time.

16
00:01:12,999 --> 00:01:15,604
Do you have an idea
what this is?

17
00:01:20,308 --> 00:01:25,075
It's a huge truck, it's a medical sign
on it, so this is a kind of mobile hospital.

18
00:01:25,643 --> 00:01:29,792
Mobile hospital which you can use if
other hospitals are broken down

19
00:01:29,792 --> 00:01:31,374
or if there are no hospitals.

20
00:01:31,781 --> 00:01:36,614
Do you have an idea how much
such an hospital would cost you

21
00:01:36,614 --> 00:01:37,873
to buy?

22
00:01:39,946 --> 00:01:43,238
3 millions, 2 millions, 3 millions,
4 millions, hum…

23
00:01:46,116 --> 00:01:49,707
This is a picture of a destroyed hospital
in Syria.

24
00:01:50,912 --> 00:01:56,571
You all see a lot of pictures on the TV,
stuff like that.

25
00:01:57,099 --> 00:02:00,901
Can you relate these 3 to 4 millions
mobile hospitals

26
00:02:00,901 --> 00:02:02,303
to these destroyed hospitals?

27
00:02:02,669 --> 00:02:06,289
Do you normally see in the media that
if a hospital is destroyed

28
00:02:06,289 --> 00:02:10,393
then this fancy stuff is deployed
to there?

29
00:02:12,697 --> 00:02:14,414
Do you have an idea why?

30
00:02:17,180 --> 00:02:18,812
It's "fucking expensive"?

31
00:02:19,545 --> 00:02:20,358
Something more?

32
00:02:25,802 --> 00:02:27,529
"Who's gonna pay for it?"

33
00:02:31,162 --> 00:02:32,878
"Is help wanted?"

34
00:02:33,454 --> 00:02:33,820
More?

35
00:02:43,250 --> 00:02:45,321
It is a question of safety from time
to time.

36
00:02:46,017 --> 00:02:51,947
But on top you have to imagine Syria,
war country, dust, heat,

37
00:02:51,947 --> 00:02:55,685
no supply chain, nothing like that, so
bringing there

38
00:02:55,685 --> 00:03:00,370
a 4 million mobile hospital might end in
two weeks of working and after that

39
00:03:00,370 --> 00:03:03,954
nothing is working anymore because
you don't have the technicians

40
00:03:03,954 --> 00:03:07,527
who can repair this kind of stuff
for example.

41
00:03:09,597 --> 00:03:11,774
Next example, I'm sure you know

42
00:03:11,875 --> 00:03:13,825
What it is is a pretty fancy fire truck.

43
00:03:14,396 --> 00:03:18,002
You have them in Germany
in all the bigger villages and

44
00:03:18,002 --> 00:03:19,551
all the cities, stuff like that.

45
00:03:20,127 --> 00:03:22,243
If you take a look inside this firetruck,

46
00:03:22,243 --> 00:03:24,927
you see plenty of fancy stuff.

47
00:03:25,285 --> 00:03:29,237
I love this stuff, there is stuff to put
out fire, there's stuff to lift things

48
00:03:29,237 --> 00:03:30,405
with hydrolics.

49
00:03:30,861 --> 00:03:34,073
There is normal stuff like shovels and
stuff like that.

50
00:03:35,657 --> 00:03:38,539
But if you relate this to pictures
in disaster areas,

51
00:03:38,539 --> 00:03:41,904
this is from Haiti, then you see that
you have plenty of people

52
00:03:41,904 --> 00:03:43,708
but no equipment at all.

53
00:03:45,571 --> 00:03:51,462
All's pretty clear because normally,
after a disaster,

54
00:03:51,462 --> 00:03:55,607
people from all over the world come
as fast as possible to the disaster area

55
00:03:55,607 --> 00:03:57,596
the so-called "urban search and rescue
teams".

56
00:03:57,963 --> 00:04:01,051
They're coming by plane, so
all the fancy stuff that we have

57
00:04:01,051 --> 00:04:02,963
in our societies stays here,

58
00:04:02,963 --> 00:04:07,104
and a few people, typically "white knights
of humanitarian aid"

59
00:04:07,288 --> 00:04:10,823
go to the disaster areas to help
"the poor people".

60
00:04:12,556 --> 00:04:14,630
You understand this was cynical.

61
00:04:16,665 --> 00:04:20,452
And there's a third example.
Have you ever seen what this is?

62
00:04:22,157 --> 00:04:23,695
This is a tourniquet.

63
00:04:24,394 --> 00:04:29,346
A tourniquet is one meter of nylon strap
and a little bit of plastic.

64
00:04:30,333 --> 00:04:35,087
A tourniquet is the best way to stop
severe bleeding immediately.

65
00:04:35,899 --> 00:04:37,974
We know this since the second world war.

66
00:04:38,033 --> 00:04:41,468
At least it's twenty years
it's totally clear and

67
00:04:41,468 --> 00:04:44,844
it's validated, this is the best way to
stop severe bleeding.

68
00:04:46,182 --> 00:04:50,449
Spoiler, you won't see this in the media
if you see like people in Syria getting hurt

69
00:04:50,449 --> 00:04:53,292
like losing legs after explosions and
stuff like that.

70
00:04:55,327 --> 00:04:59,071
Do you have an idea how expensive is
such a thing?

71
00:04:59,270 --> 00:05:02,275
One meter of nylon, a little piece
of plastic?

72
00:05:08,016 --> 00:05:10,818
Not so expensive, but it goes in
this direction:

73
00:05:10,818 --> 00:05:12,896
55 dollars for one of these.

74
00:05:12,988 --> 00:05:16,341
55 dollars for fucking one meter
of nylon strap.

75
00:05:16,858 --> 00:05:19,994
And, I don't know, less than 10g
of plastic.

76
00:05:21,614 --> 00:05:24,172
The last example, I don't know if you have
a pet.

77
00:05:24,456 --> 00:05:32,716
I had a dog, if I would like to,
I could add a GPS to my dog

78
00:05:32,716 --> 00:05:36,213
and this GPS would say when my dog
is sleeping, where my dog is,

79
00:05:36,213 --> 00:05:38,771
where I can find my dog if it's gone,
stuff like that.

80
00:05:39,582 --> 00:05:44,375
But again if you went Haiti directly after
the earthquake,

81
00:05:44,375 --> 00:05:48,802
people searched for other people in
collapsed building

82
00:05:48,875 --> 00:05:50,629
with their bare hands.

83
00:05:51,273 --> 00:05:55,628
So, obviously, we have a lot of
technical solutions for everything.

84
00:05:56,522 --> 00:06:00,830
I can look for my fucking dog in Hamburg
where it is via my app on my iPhone

85
00:06:00,830 --> 00:06:05,903
but on the other hand, in a disaster area
it's not even possible to search for

86
00:06:05,903 --> 00:06:08,965
people who are buried in collapsed buildings.

87
00:06:09,329 --> 00:06:10,795
There are several reasons for that.

88
00:06:12,014 --> 00:06:15,591
One reason: humanitarian work,
humanitarian aid is a market.

89
00:06:16,445 --> 00:06:18,513
This is a little bit perverted but
it is a market.

90
00:06:18,841 --> 00:06:20,107
It's always a question.

91
00:06:20,107 --> 00:06:22,990
It's not like "Who wants to aid?" but
"Who pays for the help?".

92
00:06:24,649 --> 00:06:26,476
"Who gets his share out of this help?"

93
00:06:28,791 --> 00:06:34,155
And you can imagine we have, I don't know,
every two or three years a major earthquake

94
00:06:34,155 --> 00:06:39,242
so the market, if you compare it to
another business market, is pretty small.

95
00:06:39,890 --> 00:06:45,739
Who cares for the 1000, 2000, 3000 people
who die in the earthquake every 3 years?

96
00:06:47,487 --> 00:06:49,762
The next thing: access to the market.

97
00:06:50,660 --> 00:06:52,572
Is it possible to bring things to Syria?

98
00:06:52,572 --> 00:06:55,165
Why should I develop, as a businessman
in capitalism,

99
00:06:55,165 --> 00:06:58,408
why should I develop something if I can't
reach my market easily?

100
00:06:59,986 --> 00:07:00,986
To make my share.

101
00:07:02,425 --> 00:07:05,332
And the third thing: who are the players
on the market.

102
00:07:05,636 --> 00:07:08,238
In humanitarian aid, most players are NGOs.

103
00:07:09,131 --> 00:07:13,119
NGOs are not really interested in
developing new things because

104
00:07:13,119 --> 00:07:18,526
if I have an NGO, a classical NGO, then
I like the things how they are.

105
00:07:19,134 --> 00:07:24,131
If there is a disaster, I send my people,
I make some nice pictures for the media

106
00:07:24,131 --> 00:07:25,760
and I get a little of donations.

107
00:07:25,924 --> 00:07:27,832
I'm not interested in changing things.

108
00:07:28,607 --> 00:07:31,690
If I would like building capacity and
local communities

109
00:07:31,690 --> 00:07:34,463
so much that they don't need me anymore
after disasters,

110
00:07:34,463 --> 00:07:37,516
then there's no need for my nice wide
NGO anymore.

111
00:07:37,961 --> 00:07:42,951
So, these three things together make
the situation where you have

112
00:07:42,951 --> 00:07:47,671
a lot of solutions in our communities
and in our societies

113
00:07:47,671 --> 00:07:52,430
but you have no possibility to bring this
to disaster aid.

114
00:07:53,608 --> 00:07:57,390
We started in 2014 in northern Syria
more or less by accident.

115
00:07:57,871 --> 00:08:03,087
We were asked if we could come
with a political delegation and make

116
00:08:03,087 --> 00:08:06,422
an overview of the medical infrastructure.

117
00:08:07,437 --> 00:08:09,630
And ever since we were stuck
in this region

118
00:08:09,630 --> 00:08:12,964
because we saw no NGOs working there,

119
00:08:12,964 --> 00:08:16,582
because the states were not really willing
to pay money for that,

120
00:08:16,582 --> 00:08:22,556
because the northern, north-east Syria
is ruled by some Kurdish militias

121
00:08:22,556 --> 00:08:26,261
and these Kurdish militias are too lefty
to get money from states,

122
00:08:26,261 --> 00:08:27,719
let's say it this way.

123
00:08:28,296 --> 00:08:37,001
So we saw the situation there and
we still had an old 4-wheel driven truck

124
00:08:37,477 --> 00:08:39,305
here in Germany and we said

125
00:08:39,305 --> 00:08:43,977
"This would be great if we just built
out of this truck a mobile hospital."

126
00:08:44,180 --> 00:08:45,280
We had no idea how to do this,

127
00:08:45,280 --> 00:08:48,980
we were really a little bit naive
in these times.

128
00:08:49,346 --> 00:08:50,360
We said just like

129
00:08:50,360 --> 00:08:54,226
"It's a nice truck, there's a lot of space
in this truck, so let's build it."

130
00:08:54,459 --> 00:08:58,367
"We have an idea, we have a fantasy.
We will go with this truck to northern Syria

131
00:08:58,367 --> 00:09:01,823
then we will give it to a local NGO and
then they have a mobile hospital

132
00:09:01,823 --> 00:09:06,374
to follow the front lines in their fight
against the so-called islamic state."

133
00:09:07,227 --> 00:09:14,177
And one year and a half, two years later,
we really were in nothern Iraq,

134
00:09:14,177 --> 00:09:17,706
not northern Syria so far, but in
northern Iraq with our mobile hospital.

135
00:09:19,745 --> 00:09:25,303
And it was pretty hard to cross a border
to Syria so we had to stay in northern Iraq

136
00:09:25,303 --> 00:09:29,005
So we asked to WHO, the World Health
Organisation,

137
00:09:29,005 --> 00:09:32,254
"What can we do right now? We are here,
we have a mobile hospital.

138
00:09:32,254 --> 00:09:33,755
Do you see any need for us?"

139
00:09:33,755 --> 00:09:34,609
And they said

140
00:09:34,609 --> 00:09:38,218
"Yeah, guys, if you'd like to, then
we would like to send you to Mossoul."

141
00:09:40,529 --> 00:09:43,499
I don't know if you saw in the media,
last year,

142
00:09:43,499 --> 00:09:47,728
the battle of Mossoul was one of the most
bloody and the most shitty battle

143
00:09:47,728 --> 00:09:51,504
that we had in the past 20, 30 years,
I think.

144
00:09:51,748 --> 00:09:57,220
It was a lot civilian casualties and
we said

145
00:09:57,220 --> 00:10:01,495
"Ok, let's try. We built this mobile
hospital, let's see if it's working."

146
00:10:03,034 --> 00:10:07,673
We were able to work, like, 1.5km
behind the frontline with the islamic state

147
00:10:07,673 --> 00:10:12,391
and we were really wondering, we were
really surprised, because we saw that

148
00:10:12,391 --> 00:10:17,352
nobody else was working there, because
they just didn't have the equipment for that

149
00:10:17,676 --> 00:10:21,273
because it is easier to buy a mobile
hospital for 3-4 million dollars

150
00:10:22,167 --> 00:10:25,014
nobody had that, and other things
were not available on the market

151
00:10:25,177 --> 00:10:27,780
so we were the only ones working there

152
00:10:28,388 --> 00:10:32,613
Over the few months that we worked
directly at the front line,

153
00:10:32,613 --> 00:10:35,583
we treated several thousands people
with this.

154
00:10:38,915 --> 00:10:43,706
This was actually the moment when
the crisis response makerspace was born.

155
00:10:44,145 --> 00:10:47,341
So it was not planned that we build up
this makerspace, it was just like

156
00:10:47,871 --> 00:10:50,717
we had two workshops in Berlin where
we fixed the truck

157
00:10:51,087 --> 00:10:54,055
where we renovated the truck and
stuff like that.

158
00:10:57,305 --> 00:10:59,944
After we went to Iraq, we just thought

159
00:10:59,944 --> 00:11:02,344
"This was a pretty good idea and
it worked out pretty well."

160
00:11:02,831 --> 00:11:07,147
We were sure there were more problems
that had to be solved in humanitarian crisis.

161
00:11:09,133 --> 00:11:13,961
Actually, this first mobile hospital went
over the border to Syria one week ago

162
00:11:13,961 --> 00:11:17,080
so it is on ??? should have gone too.

163
00:11:18,218 --> 00:11:19,723
What we're doing at the moment.

164
00:11:21,107 --> 00:11:24,756
We have some lessons learned from
this first mobile hospital and

165
00:11:24,756 --> 00:11:27,417
we're developing a second mobile hospital

166
00:11:27,417 --> 00:11:32,401
because the first one was based on
old trucks that we could get really cheap

167
00:11:32,401 --> 00:11:34,516
in Germany, but our idea was always

168
00:11:34,516 --> 00:11:39,912
we would like to create opensource
blueprints for local NGOs,

169
00:11:39,912 --> 00:11:43,233
so that they can copy our solution
that we developed.

170
00:11:44,095 --> 00:11:48,238
With these german trucks, it's not really
easy, we can't to an NGO in the Middle East

171
00:11:48,238 --> 00:11:50,641
and say "Look, this is how we build it"

172
00:11:50,885 --> 00:11:53,038
because they can't get the hand on
these german trucks

173
00:11:53,038 --> 00:11:57,267
So now we use UC containers because
you can get them everywhere in the world,

174
00:11:57,267 --> 00:12:02,345
they are unbelievably cheap, you can get
them for 2,500€ and

175
00:12:05,075 --> 00:12:08,376
at the moment in Berlin we are building
something like this.

176
00:12:10,730 --> 00:12:15,530
It's not to work inside the containers but
to have an inflatable tent structure

177
00:12:15,530 --> 00:12:19,318
coming out of the container and then
you have your 20 treatment places and

178
00:12:19,318 --> 00:12:23,907
we're working on different solutions that
are not existing at the moment

179
00:12:23,907 --> 00:12:26,712
like this patient treatment places are
based on flight cases

180
00:12:26,712 --> 00:12:27,646
and stuff like this

181
00:12:27,646 --> 00:12:32,200
We try to bring together our experiences
from the musical subculture

182
00:12:32,200 --> 00:12:37,284
from organizing festivals, from building
up structures pretty fast

183
00:12:37,284 --> 00:12:41,508
and empty rooms and stuff like that
together with this humanitarian problems.

184
00:12:44,638 --> 00:12:47,728
Another problem that I had at the beginning
was the firetruck,

185
00:12:47,728 --> 00:12:51,141
when I said "no firetruck in disaster
areas" and we thought

186
00:12:53,930 --> 00:12:56,325
"We can get our hands on these firetrucks"

187
00:12:56,458 --> 00:13:00,270
This firetruck is basically just a truck
with a good solution about

188
00:13:00,727 --> 00:13:02,679
how to store your equipment

189
00:13:02,924 --> 00:13:05,848
so that you can use your equipment
when it's still on the truck

190
00:13:05,848 --> 00:13:09,423
and you have your engine running and
you have electricity and stuff like that.

191
00:13:10,033 --> 00:13:13,406
We were thinking "Ok, you don't need
this firetruck, you just need

192
00:13:13,406 --> 00:13:17,554
a box system that you can bring with
normal appliances to disaster areas

193
00:13:17,554 --> 00:13:19,219
and then put them on normal pick-ups,

194
00:13:19,219 --> 00:13:21,409
pick-ups you find all over the world.

195
00:13:22,829 --> 00:13:28,269
For this we would need a kind of
box system that you can connect and

196
00:13:28,269 --> 00:13:30,009
three angles.

197
00:13:30,299 --> 00:13:32,780
We took a look at the market, and again,

198
00:13:32,780 --> 00:13:35,179
it's not necessary normally, to have
something like this.

199
00:13:35,341 --> 00:13:42,169
Nobody developed this, meaning that you can
load these boxes with up to 200kg.

200
00:13:44,675 --> 00:13:51,485
We developed a new kind of box. It won't
have the name CAbox in the future,

201
00:13:51,485 --> 00:13:53,681
but we couldn't find another.

202
00:13:54,250 --> 00:13:56,687
This box is a modular system.

203
00:13:58,962 --> 00:14:04,164
We try to work together with universities,
this was together with the HTW in Berlin.

204
00:14:06,764 --> 00:14:12,337
This is how things are done with us, we
have first an idea, like

205
00:14:12,337 --> 00:14:16,323
"How can we cut a firetruck in pieces,
bring it to a disaster area and

206
00:14:16,323 --> 00:14:17,825
put it together again?"

207
00:14:18,102 --> 00:14:20,625
and out of this process are coming
more and more ideas.

208
00:14:20,625 --> 00:14:24,896
We thought "Perhaps it's not only
connecting these boxes,

209
00:14:24,896 --> 00:14:28,759
perhaps it's an idea to make it in
a modular way because then,

210
00:14:28,759 --> 00:14:32,664
you can define, for every side of the box

211
00:14:32,664 --> 00:14:35,264
a special use, like for example photovoltaic

212
00:14:35,264 --> 00:14:40,822
and now we have a box developed
that you can put in a normal airplane,

213
00:14:40,822 --> 00:14:45,742
bring to a disaster area, just turn around
these boxes, these sides of the boxes

214
00:14:45,742 --> 00:14:50,138
and get electricity, for example inside
a water pump or something like that.

215
00:14:52,779 --> 00:14:57,820
We then thought about "Ok, but how
can we get into the disaster areas

216
00:14:57,820 --> 00:14:59,323
if there are lots of blocks"

217
00:14:59,323 --> 00:15:02,115
We saw this in the Nepal
after the earthquake,

218
00:15:02,115 --> 00:15:06,013
we saw this on Puerto Rico last year
after the typhoon,

219
00:15:06,013 --> 00:15:08,341
that the help couldn't reach the island

220
00:15:09,234 --> 00:15:13,540
We thought that it's not so difficult
to throw things out of airplanes.

221
00:15:14,882 --> 00:15:16,631
So again, we took a look at the market
and said

222
00:15:16,631 --> 00:15:23,093
"Wow, no civilian solutions to throw
huge payloads out of aircrafts."

223
00:15:24,637 --> 00:15:27,603
The solutions on the market are
military solutions.

224
00:15:28,529 --> 00:15:33,602
So the UN can use it, but normally
it's up to the national military

225
00:15:33,845 --> 00:15:34,984
who can use it.

226
00:15:35,269 --> 00:15:38,566
The only solution we could find was
a box system coming

227
00:15:38,566 --> 00:15:41,571
from an english company

228
00:15:41,571 --> 00:15:49,937
and with this box, you can throw, I think,
up to 70kg but only things that can't break

229
00:15:50,506 --> 00:15:53,633
you can't throw out medical instruments
with this.

230
00:15:55,263 --> 00:15:58,468
So, what we did, we called again,
we looked for

231
00:15:58,468 --> 00:16:02,940
who could be the best person to talk
with us about something like that.

232
00:16:03,673 --> 00:16:06,109
And we thought, it's not the military,
obviously, because

233
00:16:06,109 --> 00:16:11,183
they're thinking bigger scales, they're
thinking endless money,

234
00:16:11,183 --> 00:16:13,047
they're thinking endless logistics

235
00:16:13,047 --> 00:16:16,821
So we got together with paragliders
from Switzerland.

236
00:16:18,446 --> 00:16:22,097
We met with the refugee response
in the Mediterranean

237
00:16:22,632 --> 00:16:26,211
and told them "We would like
to throw things out of airplanes.

238
00:16:26,822 --> 00:16:28,204
What do you think about it?"

239
00:16:28,571 --> 00:16:32,106
They said "Why not? We are jumping
out of airplanes all the time!"

240
00:16:32,592 --> 00:16:35,109
So why should it be so difficult?

241
00:16:35,726 --> 00:16:40,516
What came out is that every paraglider
which goes down a mountain

242
00:16:40,516 --> 00:16:46,253
in Switzerland, frequently, daily, has
an emergency parachute

243
00:16:46,416 --> 00:16:47,483
in his powerglide.

244
00:16:47,804 --> 00:16:51,500
And this emergency parachute has to be
renewed every 3 years,

245
00:16:51,829 --> 00:16:57,282
not because it's broken, but because we're
living in a rich society and insurances say

246
00:16:57,282 --> 00:16:59,723
"If you don't do this, I won't insure you."

247
00:17:00,740 --> 00:17:08,450
So, you have plenty, hundreds, thousands
of used parachutes that are totally ok

248
00:17:08,694 --> 00:17:12,108
And you can hang, obviously, more than
100kg on them.

249
00:17:12,516 --> 00:17:14,184
Then you have these tandem parachutes,

250
00:17:14,184 --> 00:17:17,069
you can obviously hang more than 250kg
on them.

251
00:17:18,863 --> 00:17:21,955
So, what we did, totally illegal,

252
00:17:22,321 --> 00:17:26,180
we tried it out and threw some things
from really high bridges in Switzerland

253
00:17:26,465 --> 00:17:28,134
with these parachutes.

254
00:17:28,697 --> 00:17:31,057
And then we saw "yeah, it's
functioning."

255
00:17:32,897 --> 00:17:35,262
Next thing was that we developed

256
00:17:35,385 --> 00:17:36,805
[laughter]

257
00:17:36,933 --> 00:17:42,993
a kind of absorption with carton

258
00:17:44,943 --> 00:17:49,539
and the funny thing when we went
to Switzerland and said to the officials

259
00:17:49,539 --> 00:17:54,534
"Yeah, you know, we're a bunch of
punk rock idiots working in

260
00:17:54,534 --> 00:17:57,869
humanitarian aid but we have this idea,
we could throw things

261
00:17:57,869 --> 00:17:59,817
out of normal parachuting machines,

262
00:17:59,817 --> 00:18:01,887
because you can find them all over
the world,

263
00:18:01,887 --> 00:18:03,756
because all over the world,
people do this sport.

264
00:18:04,367 --> 00:18:09,372
And it's pretty cheap, and we could reach
valleys that are not reachable yet".

265
00:18:09,860 --> 00:18:10,920
They said

266
00:18:10,920 --> 00:18:12,627
"Do you know what the problem is?

267
00:18:12,627 --> 00:18:15,915
Every country has to have this air drop
capacity."

268
00:18:17,252 --> 00:18:25,575
This is like, I don't know, regularly
from the international air travel something

269
00:18:25,575 --> 00:18:30,490
and they said "We, in Switzerland,
we solve this again with the military.

270
00:18:30,490 --> 00:18:35,414
We have to pay like 24,000€ an hour
to do this because we do this

271
00:18:35,414 --> 00:18:37,369
with helicopters.

272
00:18:37,613 --> 00:18:41,909
You offer us a solution that, if it's
working out, is working with

273
00:18:41,909 --> 00:18:44,761
2,000€ an hour."

274
00:18:45,012 --> 00:18:50,372
So they said "If your system is working,
you will be immediately assigned

275
00:18:50,372 --> 00:18:54,846
as an official humanitarian air drop
capacity in Switzerland."

276
00:18:56,922 --> 00:19:00,911
We started 1.5 years ago and then

277
00:19:04,157 --> 00:19:05,784
what's called ???

278
00:19:08,548 --> 00:19:14,365
building in Berlin to renew old trucks
to bring to Northern Syria

279
00:19:15,177 --> 00:19:19,168
and then we thought "Let's use this space,
let's look for solutions

280
00:19:19,168 --> 00:19:20,633
for humanitarian problems."

281
00:19:21,526 --> 00:19:26,650
And the idea was, in the beginning, just
to bring together nerds, geeks, people

282
00:19:26,650 --> 00:19:32,341
out of the field, refugees who know best
what will help them in the crises,

283
00:19:32,341 --> 00:19:35,595
specialists, universities, stuff like that.

284
00:19:35,838 --> 00:19:40,271
What started like "Let's see
if it could happen."

285
00:19:40,271 --> 00:19:46,525
comes out as, this year in July, we will
throw out some boxes with parachutes

286
00:19:46,525 --> 00:19:49,696
out of airplanes and we'll perhaps have

287
00:19:49,696 --> 00:19:53,320
the first worldwide airborne emergency
response unit.

288
00:19:56,568 --> 00:19:59,819
So, yes, we saw there is a big need
for these things.

289
00:20:00,388 --> 00:20:06,687
At the moment, we don't have a lot of
IT projects, we have some ideas and

290
00:20:06,687 --> 00:20:13,356
we would like to grow our network,
because we have nerds from the CCC

291
00:20:13,356 --> 00:20:17,730
sitting around there, but at the moment
they love to do something with ???

292
00:20:23,686 --> 00:20:28,117
but this makerspace is a makerspace like
you know it.

293
00:20:28,777 --> 00:20:31,013
You have an open space, everybody could
come in.

294
00:20:31,378 --> 00:20:33,667
We have only the regular toys
and we say

295
00:20:33,668 --> 00:20:37,808
"Everything that is developed should be
with a focus on humanitarian aid and

296
00:20:38,074 --> 00:20:40,801
everything that we develop has to be
open source

297
00:20:41,411 --> 00:20:44,615
so that people can use it worldwide."

298
00:20:46,286 --> 00:20:48,969
If you would like to get in contact
with us,

299
00:20:48,969 --> 00:20:51,526
www.cadus.org

300
00:20:51,526 --> 00:20:57,341
We have all this fancy stuff like facebook,
twitter and that you can find

301
00:20:57,341 --> 00:20:59,170
on the homepage.

302
00:21:00,554 --> 00:21:02,628
Thank you very much for your attention.

303
00:21:05,336 --> 00:21:11,261
[Applause]

304
00:21:15,414 --> 00:21:17,202
Are there questions?

305
00:21:27,568 --> 00:21:33,167
[Q] Hi, thank you for the talk. I'm not sure
if you know about an organization,

306
00:21:33,167 --> 00:21:37,725
I think it's only french at the moment,
which is called HAND, which is

307
00:21:37,725 --> 00:21:40,444
Hackers Against Natural Disasters

308
00:21:40,444 --> 00:21:46,533
It's basically a non-profit which tries
to help local populations

309
00:21:46,533 --> 00:21:53,576
in case of natural disasters, mostly
they were in… for the typhoon

310
00:21:53,576 --> 00:21:56,546
in french islands in the Carribean

311
00:21:56,546 --> 00:22:02,195
and they tried to set up some kind of
infrastructures, IT infrastructures.

312
00:22:02,195 --> 00:22:07,803
There are ham radios and they tried to
set up internet and stuff like that so

313
00:22:07,803 --> 00:22:11,297
people can actually use communications

314
00:22:11,297 --> 00:22:14,268
and they tried to set up like 3G networks.

315
00:22:16,328 --> 00:22:18,781
I think it's only french but they have
a great,

316
00:22:19,043 --> 00:22:23,811
I don't know a lot about them, but I think
they have a quite hacker spirit.

317
00:22:25,510 --> 00:22:30,555
It might be a helpful cooperation
with you.

318
00:22:31,613 --> 00:22:33,888
[A] This is really perfect, thank you
very much.

319
00:22:34,946 --> 00:22:37,957
There are actually a lot of groups
like this worldwide.

320
00:22:38,273 --> 00:22:43,795
For example, we are now part of the GIG,
the Global Innovation Gathering which are

321
00:22:43,795 --> 00:22:45,714
50 maker spaces all over the world.

322
00:22:46,363 --> 00:22:49,903
There are small organizations that are
really interesting.

323
00:22:50,758 --> 00:22:56,896
The problem is, it's hard to find
your space in this humanitarian world

324
00:22:56,896 --> 00:23:01,893
because big organizations really
close down the access to this,

325
00:23:01,893 --> 00:23:04,968
the access to big amounts of money,

326
00:23:04,968 --> 00:23:10,791
so contacts are always welcome because
I think we can only reach something

327
00:23:10,994 --> 00:23:14,937
if we form better and bigger networks.

328
00:23:23,100 --> 00:23:29,523
[Q] In areas of crisis, and especially
in war, it's hard to understand

329
00:23:29,523 --> 00:23:35,249
which interests are on which side or
are there any goods in this.

330
00:23:35,494 --> 00:23:37,891
How to you avoid to become
a useful idiot?

331
00:23:39,839 --> 00:23:41,958
[A] I think…

332
00:23:47,159 --> 00:23:52,001
I don't avoid it and humanitarians are
a kind of useful idiots.

333
00:23:52,689 --> 00:23:56,266
In the humanitarian sector, there are
two main pictures about

334
00:23:56,266 --> 00:23:57,772
humanitarian aid.

335
00:23:58,214 --> 00:24:01,926
It was Henri Dunant who founded
the red cross, who said like

336
00:24:01,926 --> 00:24:07,656
"Yes, there is a war. Yes, obviously
these are assholes that shoot at each other

337
00:24:07,656 --> 00:24:09,892
but in the end they're humans.

338
00:24:10,214 --> 00:24:13,470
So once they're laying down on the floor,
they deserve to be treated like humans.

339
00:24:13,470 --> 00:24:17,247
I know that if I treat him and
he's fit again,

340
00:24:17,247 --> 00:24:19,236
perhaps he goes out again and
shoots again."

341
00:24:20,170 --> 00:24:24,031
And there was the other person,
Florence Nightingale at the same time.

342
00:24:24,356 --> 00:24:28,137
She said, like, "Humanitarian aid
should mean 'only these people

343
00:24:28,137 --> 00:24:33,376
get humanitarian aid who bond themselves
to these humanitarian principles'.

344
00:24:33,904 --> 00:24:37,763
So, a soldier that got shot down, I won't
treat him if I'm not sure

345
00:24:37,763 --> 00:24:38,980
that he won't go out again."

346
00:24:41,373 --> 00:24:47,148
In the meanwhile, I think the humanitarian
world stuck totally to the model of Dunant

347
00:24:47,148 --> 00:24:48,847
and for us it's the same.

348
00:24:49,100 --> 00:24:52,227
I treated people who fought for ISIS
in Mossoul definitely.

349
00:24:53,534 --> 00:24:57,068
Because in my opinion, if I go there
and say

350
00:24:57,068 --> 00:25:04,304
"I am the medic and the judge at the same
time", then it's really strange.

351
00:25:04,791 --> 00:25:07,956
There have to be other people to judge
over this.

352
00:25:09,747 --> 00:25:14,164
As a humanitarian aid worker, you are
little bit helpful idiot all the way.

353
00:25:15,661 --> 00:25:19,661
The other discussion is definitely
a question about

354
00:25:19,661 --> 00:25:25,059
if humanitarian aid brings anything or
if it's just like a machine

355
00:25:25,059 --> 00:25:28,440
that tries to run itself again and again
and again…

356
00:25:29,009 --> 00:25:30,556
Like this help industry.

357
00:25:31,490 --> 00:25:36,441
This, we try to avoid with not playing
after the same rules.

358
00:25:37,628 --> 00:25:41,039
We are not into, I don't know.

359
00:25:41,405 --> 00:25:46,564
Every 2 or 3 years there is a new theme
that you have to surf.

360
00:25:46,897 --> 00:25:51,895
For example, now it's capacity building
in war zones.

361
00:25:53,358 --> 00:25:57,260
We think it's pretty easy, we don't have
to follow these rules of

362
00:25:57,260 --> 00:26:00,215
the humanitarian sector, the humanitarian
market.

363
00:26:00,537 --> 00:26:04,407
We stuck with the humanitarian ideals
and we always try to work together

364
00:26:04,407 --> 00:26:05,829
with the locals.

365
00:26:06,276 --> 00:26:09,759
And, yes, for sure, you always have locals
who try to have their share

366
00:26:09,759 --> 00:26:13,872
out of these crises, but if you talk
to the locals, talk to different locals

367
00:26:13,872 --> 00:26:18,102
several organizations together, you will
see who's really working on the ground

368
00:26:18,102 --> 00:26:21,153
just to help the people and we try to
support these.

369
00:26:22,573 --> 00:26:29,197
Like I said, we don't produce or develop
solutions that we can sell.

370
00:26:29,512 --> 00:26:34,729
It's all open source, so I hope it helps
to avoid a little bit

371
00:26:34,729 --> 00:26:37,196
to support the wrong people.

372
00:26:40,008 --> 00:26:41,754
Any further questions?

373
00:26:44,028 --> 00:26:48,166
[Q] Hi, awesome work. Have you considered
3D printing to maybe come up with

374
00:26:48,166 --> 00:26:49,876
spare parts or let…

375
00:26:49,876 --> 00:26:50,939
[A] Sorry, again?

376
00:26:50,939 --> 00:26:52,171
[Q] Have you considered 3D printing?

377
00:26:52,751 --> 00:26:56,974
[A] Yes, there are actually organizations
who are doing this.

378
00:26:58,279 --> 00:27:01,522
There one really cool organization
called Field Ready.

379
00:27:03,067 --> 00:27:06,971
They're developing a database with
a lot of things like

380
00:27:06,971 --> 00:27:12,323
the small thing you need to stop the
connection between a mother

381
00:27:12,323 --> 00:27:14,802
and a baby, I don't know what the name
is in english.

382
00:27:15,293 --> 00:27:20,213
And they develop a database so that
people can print these medical devices

383
00:27:20,213 --> 00:27:22,359
in disaster areas, that's pretty cool.

384
00:27:24,632 --> 00:27:28,041
At some moments, we tried, a few months ago

385
00:27:28,041 --> 00:27:31,796
to get our hands on 3D printers but then
it's so expensive at the moment and

386
00:27:31,796 --> 00:27:36,997
we are just more experienced with metal
and steel and stuff like that.

387
00:27:37,281 --> 00:27:43,625
But we looked into this and at the moment
we're always doing too much unfortunately.

388
00:27:46,392 --> 00:27:50,122
I wanted to buy one and then our colleagues
from CCC said

389
00:27:50,122 --> 00:27:53,455
"Don't do that, you're not clever enough",
so…

390
00:27:55,201 --> 00:27:58,003
"You'll destroy it! Waste money!"

391
00:28:00,970 --> 00:28:02,915
But actually, for the tourniquet for example

392
00:28:02,915 --> 00:28:05,361
there is a solution as well for 3D printing.

393
00:28:05,642 --> 00:28:07,439
I just read an article a few days ago.

394
00:28:08,246 --> 00:28:13,083
I think 3D printing is a kind of…
really close connected

395
00:28:13,083 --> 00:28:15,315
to the future of humanitarian aid,
definitely.

396
00:28:18,441 --> 00:28:20,106
Any further questions?

397
00:28:27,583 --> 00:28:29,696
[Q] It's less question, more comment.

398
00:28:30,589 --> 00:28:35,916
Because you showed that we have these
cool sexy solutions for technology

399
00:28:35,916 --> 00:28:40,309
for finding dogs and in less developed
countries we have problems, but

400
00:28:40,309 --> 00:28:44,859
it's also, like, we have problems with
disasters in all the world.

401
00:28:45,551 --> 00:28:49,080
Look at what happened in New Orleans,
look at what is still happening

402
00:28:49,080 --> 00:28:53,266
in Puerto Rico, which is supposedly
first world country, but has still

403
00:28:53,266 --> 00:28:55,130
problems with electricity.

404
00:28:55,576 --> 00:28:58,786
It's not that there are good countries
and bad countries, but we have

405
00:28:58,786 --> 00:29:05,081
problems with all those global solutions
to recover.

406
00:29:05,773 --> 00:29:12,107
I think that's a good approach to get
local people to organize and

407
00:29:12,107 --> 00:29:15,075
to try to solve because they know
what is needed most.

408
00:29:16,701 --> 00:29:19,226
[A] Totally. I think it's a fucked up thing
about capitalism.

409
00:29:19,754 --> 00:29:23,005
If I live in a nice decent city like
Hamburg and I have a dog,

410
00:29:23,005 --> 00:29:31,005
I will easily spend 10€ a month for an app
to find my dog, but I don't relate

411
00:29:31,005 --> 00:29:35,235
to the thing that I could be on a zone
rubbish as well,

412
00:29:35,235 --> 00:29:40,020
so I won't pay for a disaster app or
something like that

413
00:29:40,020 --> 00:29:41,777
because it's not part of my daily life.

414
00:29:42,346 --> 00:29:45,192
So no company will be willing to develop
something like this,

415
00:29:45,192 --> 00:29:47,181
it's just like the rules of the market.

416
00:29:47,628 --> 00:29:49,169
Silly but it is what it is.

417
00:29:53,030 --> 00:29:54,341
Thank you very much for your attention.

418
00:29:54,669 --> 00:29:56,212
Thanks a lot Sebastian again.

419
00:29:56,536 --> 00:30:01,096
[Applause]
