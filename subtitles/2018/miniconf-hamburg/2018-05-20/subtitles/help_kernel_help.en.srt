1
00:00:06,048 --> 00:00:07,311
Without further ado

2
00:00:07,311 --> 00:00:09,219
the first talk this morning is by

3
00:00:09,219 --> 00:00:11,859
our beloved kernel maintainer Ben Hutchings

4
00:00:12,215 --> 00:00:13,812
about "Help the kernel team
to help you".

5
00:00:16,944 --> 00:00:17,998
Hi.

6
00:00:18,121 --> 00:00:24,087
[Applause]

7
00:00:24,828 --> 00:00:27,379
As Michael said, I'm one of the kernel
maintainers.

8
00:00:27,379 --> 00:00:30,479
I've been on the kernel team
for about ten years now.

9
00:00:35,519 --> 00:00:36,900
So, I'm going to talk about

10
00:00:36,900 --> 00:00:41,694
what Debian users and developers can do

11
00:00:41,694 --> 00:00:43,185
when interacting with the kernel team

12
00:00:44,363 --> 00:00:51,802
to make us more effective; more able
to deal with your requests quickly.

13
00:00:56,312 --> 00:00:57,724
We're quite busy.

14
00:00:58,910 --> 00:01:00,532
There are about a dozen people

15
00:01:00,532 --> 00:01:03,700
on the kernel team, but

16
00:01:03,700 --> 00:01:06,591
most of us have other responsibilities
within Debian.

17
00:01:13,011 --> 00:01:15,276
Most of us are not paid to work
on Debian either

18
00:01:15,276 --> 00:01:18,131
so we only have a few hours a week
to spend on it.

19
00:01:20,153 --> 00:01:22,080
We get a constant stream of bug reports

20
00:01:24,684 --> 00:01:26,143
some of which we can handle

21
00:01:26,143 --> 00:01:28,177
some of which unfortunately we can't.

22
00:01:28,951 --> 00:01:32,035
There's a large backlog of bug reports

23
00:01:32,035 --> 00:01:37,244
that probably won't get dealt with
in Debian.

24
00:01:37,528 --> 00:01:40,092
They might get fixed upstream, but

25
00:01:41,742 --> 00:01:43,503
we won't get to them.

26
00:01:46,351 --> 00:01:50,934
One of the first things you can do
to make our life easier is

27
00:01:50,934 --> 00:01:52,966
report bugs upstream.

28
00:01:55,122 --> 00:01:59,928
If you're running a recent kernel—and
that doesn't have to be absolutely

29
00:01:59,928 --> 00:02:02,720
the latest version that Linus released
yesterday;

30
00:02:04,058 --> 00:02:08,041
any version in testing, unstable,
or experimental

31
00:02:09,063 --> 00:02:11,928
or the current most recent
backports suite

32
00:02:12,212 --> 00:02:14,164
should be recent enough—

33
00:02:14,325 --> 00:02:16,354
if you're running one of those versions

34
00:02:16,354 --> 00:02:20,952
then the upstream kernel developers
would probably be quite pleased to

35
00:02:20,952 --> 00:02:22,821
receive your bug report.

36
00:02:24,650 --> 00:02:28,903
Some subsystems in the kernel use
a bug tracker like Bugzilla

37
00:02:31,333 --> 00:02:33,499
and many do not—

38
00:02:33,759 --> 00:02:35,577
they just want bug reports directly

39
00:02:35,783 --> 00:02:38,103
to their development mailing list.

40
00:02:38,633 --> 00:02:42,492
There's a documentation file called
MAINTAINERS

41
00:02:42,774 --> 00:02:44,195
which we package

42
00:02:44,480 --> 00:02:46,232
—you can find it in the linux-doc package—

43
00:02:46,964 --> 00:02:49,433
and that lists for each area of the kernel

44
00:02:50,330 --> 00:02:53,471
the email addresses of maintainers,

45
00:02:53,471 --> 00:02:59,486
the address of any relevant development
mailing list,

46
00:03:00,378 --> 00:03:05,468
and in some cases it will say they use a
bug tracker at this URL.

47
00:03:10,987 --> 00:03:13,548
That doesn't mean that you shouldn't
report a bug in Debian as well.

48
00:03:13,673 --> 00:03:16,395
If you report a bug in Debian and upstream

49
00:03:16,395 --> 00:03:20,822
then use the standard "forwarded" command
to link them together

50
00:03:21,350 --> 00:03:24,196
and we should then be able to see
status changes

51
00:03:25,125 --> 00:03:28,250
if you reported in Bugzilla upstream.

52
00:03:33,204 --> 00:03:36,009
Secondly, report bugs with the right
information.

53
00:03:38,166 --> 00:03:42,191
The kernel packages that we build include
some hook scripts

54
00:03:42,191 --> 00:03:44,222
for the "reportbug" command

55
00:03:44,222 --> 00:03:47,353
so it can gather some diagnostic
information

56
00:03:47,353 --> 00:03:51,987
and we generally expect that if you are
reporting a bug that is about

57
00:03:51,987 --> 00:03:55,078
"This doesn't work on my machine"

58
00:03:55,078 --> 00:03:59,261
then we want some diagnostic information
about your machine.

59
00:04:01,250 --> 00:04:04,579
Running some commands that you thought might be useful

60
00:04:05,196 --> 00:04:09,662
is not usually as good as running all the
diagnostic commands

61
00:04:10,031 --> 00:04:12,184
that are in these scripts.

62
00:04:12,713 --> 00:04:17,262
So the right way to report a bug
in the currently running kernel

63
00:04:17,262 --> 00:04:19,254
is just "reportbug kernel".

64
00:04:19,577 --> 00:04:22,619
Reportbug knows how to look up
the correct package for that.

65
00:04:25,541 --> 00:04:31,685
Otherwise, you should report against
the specific versioned package

66
00:04:32,822 --> 00:04:37,333
for example
"linux-image-4.9.0-6-amd64"

67
00:04:37,537 --> 00:04:41,603
would be the current kernel package
if you're running Stretch

68
00:04:42,213 --> 00:04:44,278
on a 64-bit PC.

69
00:04:45,178 --> 00:04:50,460
Don't file bugs against metapackages
like linux-image-amd64

70
00:04:51,173 --> 00:04:55,075
because those are basically just some
metadata saying

71
00:04:55,075 --> 00:04:58,170
"This is the current version of the kernel
and you should install that."

72
00:04:58,861 --> 00:05:02,921
Don't report bugs against firmware packages

73
00:05:02,921 --> 00:05:05,653
unless you're really sure the bug is
in firmware

74
00:05:05,653 --> 00:05:07,607
rather than in the driver.

75
00:05:09,068 --> 00:05:11,818
This may seem obvious but people do
those things.

76
00:05:16,620 --> 00:05:18,012
Adding features.

77
00:05:21,138 --> 00:05:26,300
We do have some long-standing patches
in the kernel, in the linux package.

78
00:05:29,832 --> 00:05:32,966
We don't really want to add to those.

79
00:05:33,531 --> 00:05:36,937
Most of those really ought to get
cleaned up and sent upstream

80
00:05:36,937 --> 00:05:38,731
but it requires time
to do that.

81
00:05:41,189 --> 00:05:44,771
So, new features should always
be added upstream.

82
00:05:45,380 --> 00:05:47,617
As soon as they're accepted upstream,

83
00:05:48,904 --> 00:05:55,406
we're happy to add them into the earlier
versions that we have in Debian

84
00:05:55,406 --> 00:05:57,925
because we know that if they've accepted
upstream

85
00:05:57,925 --> 00:06:01,789
then as soon as we get to that new
upstream version

86
00:06:01,789 --> 00:06:03,085
we can drop that patch

87
00:06:03,085 --> 00:06:06,867
so it's not adding to the long term
burden of maintenance.

88
00:06:10,321 --> 00:06:15,478
I've got a link there to the documentation
on how to contribute to the kernel.

89
00:06:18,861 --> 00:06:21,829
We would be very happy, well I would
be very happy

90
00:06:21,829 --> 00:06:29,444
if people would volunteer to work
on those long-standing patches

91
00:06:29,718 --> 00:06:33,998
and get them into a state where they would
be accepted upstream.

92
00:06:38,839 --> 00:06:43,316
So, you reported a bug upstream,
and it got fixed.

93
00:06:43,560 --> 00:06:44,578
That's great

94
00:06:45,185 --> 00:06:49,984
but quite often that fix isn't going
to get into a stable release

95
00:06:49,984 --> 00:06:52,617
of the kernel for several months.

96
00:06:56,961 --> 00:07:01,968
If the bug was actually found
in a stable release,

97
00:07:01,968 --> 00:07:04,244
rather than unstable or testing

98
00:07:04,244 --> 00:07:11,844
then that fix might not automatically
get into a stable update at all.

99
00:07:13,960 --> 00:07:18,385
So, you probably want to tell us
what the fix is

100
00:07:18,385 --> 00:07:20,385
so that we can apply it now.

101
00:07:21,999 --> 00:07:25,710
If you can give a reference to the
specific commit, if you know that

102
00:07:25,710 --> 00:07:27,700
that is absolutely ideal.

103
00:07:28,920 --> 00:07:33,473
We can easily then dig out that commit
and add it.

104
00:07:35,463 --> 00:07:41,518
There's a patch tracking system used by
many of the kernel mailing lists

105
00:07:41,518 --> 00:07:48,547
called "patchwork" and that will gather
together a patch and

106
00:07:48,547 --> 00:07:50,175
all the discussion about it.

107
00:07:51,143 --> 00:07:54,037
It also gathers together patch series,
which is useful

108
00:07:54,037 --> 00:07:56,158
if a fix takes multiple steps.

109
00:07:59,749 --> 00:08:02,680
If you tell us that the patch was discussed
on a mailing list and

110
00:08:02,680 --> 00:08:04,228
link to an archive

111
00:08:04,228 --> 00:08:10,162
that can work, but mailing list archives
often mangle patches

112
00:08:10,162 --> 00:08:11,993
so then we need to undo the mangling

113
00:08:11,993 --> 00:08:15,811
so that takes longer to deal with.

114
00:08:17,808 --> 00:08:22,930
If you simply send a patch to the bug tracker

115
00:08:22,930 --> 00:08:26,993
without any link to "this is where it
came from upstream",

116
00:08:26,993 --> 00:08:29,719
that's actually kind of a problem because

117
00:08:32,559 --> 00:08:36,955
we don't know whether that's really
what you say it is.

118
00:08:38,092 --> 00:08:40,287
If it's a signed mail
from a Debian Developer

119
00:08:40,287 --> 00:08:42,200
OK, yeah, we can probably trust it.

120
00:08:42,322 --> 00:08:47,809
If it's not a signed mail or it's from
a Debian user,

121
00:08:47,809 --> 00:08:51,025
then we don't know.

122
00:08:51,390 --> 00:08:53,948
So we need to actually look upstream to see

123
00:08:53,948 --> 00:08:57,286
"This is the version that got committed."

124
00:08:58,829 --> 00:09:06,149
If you want to do a backport from upstream
to whatever is the current version

125
00:09:06,149 --> 00:09:10,303
that's great, but you need to include
the upstream reference as well.

126
00:09:16,802 --> 00:09:18,837
Talk to us as a team.

127
00:09:21,189 --> 00:09:24,269
From time to time, I will get mail
directly to me, saying

128
00:09:24,919 --> 00:09:27,722
"Oh there's this bug." or
"Can you help me with this?"

129
00:09:28,574 --> 00:09:33,252
or occasionally a company saying

130
00:09:33,252 --> 00:09:36,344
"Oh, we want to update the support for
our hardware."

131
00:09:38,579 --> 00:09:40,765
They should not be mailing just me,

132
00:09:40,765 --> 00:09:45,645
they should always—almost always—be
sending bug reports

133
00:09:45,645 --> 00:09:48,244
to the regular Debian bug tracker

134
00:09:48,244 --> 00:09:52,913
and other mail should go to the
debian-kernel mailing list.

135
00:09:55,068 --> 00:10:01,813
We also have a development IRC channel,
#debian-kernel on irc.debian.org

136
00:10:01,813 --> 00:10:07,661
and some things can be discussed there.

137
00:10:07,903 --> 00:10:12,743
Usually it's best to send longer messages
as email though.

138
00:10:14,207 --> 00:10:18,839
The only reason you would want to
not use one of those public…

139
00:10:19,163 --> 00:10:23,236
The only reason why you should not use
those public channels is

140
00:10:23,236 --> 00:10:27,663
if you're discussing a security issue
that's currently not public

141
00:10:27,663 --> 00:10:31,120
and shouldn't be made public until
it's fixed

142
00:10:31,120 --> 00:10:34,651
and in that case, do contact me directly

143
00:10:34,651 --> 00:10:37,827
but also the Debian security team.

144
00:10:46,851 --> 00:10:51,797
If you want to contribute to the packaging
rather than…

145
00:10:51,797 --> 00:10:55,144
if you don't want to touch the kernel code
itself

146
00:10:55,144 --> 00:10:57,046
but you want to contribute
to the packaging

147
00:10:57,046 --> 00:10:59,640
maybe you want to change the configuration;

148
00:10:59,640 --> 00:11:06,714
maybe you want to make the packaging
more suitable for use

149
00:11:06,714 --> 00:11:09,397
by downstreams, derivatives;

150
00:11:12,484 --> 00:11:15,532
if you simply want to improve
the packaging in Debian;

151
00:11:20,093 --> 00:11:24,935
patches to that are OK,
merge requests are wonderful.

152
00:11:26,476 --> 00:11:30,501
Since we moved to Salsa and can take
merge requests

153
00:11:30,501 --> 00:11:32,901
through the Gitlab software

154
00:11:33,706 --> 00:11:38,314
I've found I can review and comment on
and apply these changes

155
00:11:39,203 --> 00:11:40,788
pretty quickly.

156
00:11:42,050 --> 00:11:44,164
It also helps that we get
a notification

157
00:11:44,164 --> 00:11:46,398
for all the new merge requests on IRC

158
00:11:46,639 --> 00:11:49,849
so that's pretty much instant.

159
00:11:51,520 --> 00:11:57,883
If a team member is looking at
the IRC channel and

160
00:11:57,883 --> 00:12:01,346
has time available then they can
deal with that

161
00:12:03,247 --> 00:12:05,411
in minutes sometimes.

162
00:12:06,585 --> 00:12:08,131
So, in the last…

163
00:12:08,131 --> 00:12:11,212
I checked back in the git history and
found in the last four weeks

164
00:12:11,212 --> 00:12:13,455
that we used Alioth,

165
00:12:13,738 --> 00:12:17,269
there appears to be only one patch
to the linux package

166
00:12:17,269 --> 00:12:23,846
that wasn't either picked from upstream
or done by a team member.

167
00:12:25,143 --> 00:12:28,027
In the last four weeks up to yesterday

168
00:12:28,027 --> 00:12:31,692
we accepted 14 merge requests on Salsa.

169
00:12:35,024 --> 00:12:45,996
So, this is a massive improvement to
the productivity of the team and

170
00:12:45,996 --> 00:12:50,177
our ability to accept outside contributions.

171
00:12:52,157 --> 00:12:56,220
Once again, a reminder that feature patches
for the kernel code itself

172
00:12:56,220 --> 00:12:58,255
do need to go to upstream first.

173
00:13:01,465 --> 00:13:03,498
So, that's about it

174
00:13:06,426 --> 00:13:11,096
but I've got about five minutes for questions.

175
00:13:14,105 --> 00:13:19,922
[Applause]

176
00:13:22,201 --> 00:13:24,151
Are there questions?

177
00:13:30,693 --> 00:13:33,015
[Q] Hi Ben, thanks for the lecture.

178
00:13:34,355 --> 00:13:38,012
I am wondering what are those long-
standing patches you mentioned.

179
00:13:39,273 --> 00:13:41,253
Can you give a few examples for that?

180
00:13:42,032 --> 00:13:46,868
[Ben] One of the things that actually
requires the most work

181
00:13:47,151 --> 00:13:48,901
when moving to a new kernel version is

182
00:13:48,901 --> 00:14:01,253
we have a patch to, firstly, add specific
log messages to the firmware loader

183
00:14:01,813 --> 00:14:04,905
so whenever a firmware file is missing

184
00:14:04,905 --> 00:14:08,238
it will always log that in a standard format.

185
00:14:08,765 --> 00:14:19,612
This is useful for the installer,
which uses that to detect missing firmware

186
00:14:19,612 --> 00:14:21,322
and warn you.

187
00:14:23,190 --> 00:14:26,317
And then, because many drivers also

188
00:14:26,317 --> 00:14:29,201
log firmware errors in inconsistent ways

189
00:14:29,201 --> 00:14:35,738
there's a second patch that removes
those redundant log messages

190
00:14:35,738 --> 00:14:38,466
and that one keeps getting conflicts
when we update.

191
00:14:38,711 --> 00:14:45,339
So really, those ought to get cleaned up
a bit and sent upstream.

192
00:14:49,248 --> 00:14:51,402
Further questions?

193
00:14:59,328 --> 00:15:01,690
Well, if not, then let's thank Ben again.
